package com.example.guirodrpc.empresaioasys.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.guirodrpc.empresaioasys.R;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
