package com.example.guirodrpc.empresaioasys.control;

import com.example.guirodrpc.empresaioasys.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginService {

    @FormUrlEncoded
           @POST("users/auth/sign_in/")
   Call<User> login(@Body User user);

}
